#### Messages from JS8Call

-   `CLOSE` - JS8Call has been closed
-   `INBOX.MESSAGE` - A single inbox message
-   `INBOX.MESSAGES` - A list of inbox messages
-   `LOG.QSO` - Details of QSO submitted to the log
-   `MODE.SPEED` - The current TX speed
-   `PING` - Keepalive from JS8Call
-   `RIG.FREQ` - The rig frequency has been changed
-   `RIG.PTT` - PTT has been toggled
-   `RX.ACTIVITY` - We received something
-   `RX.BAND_ACTIVITY` - The band activity window
-   `RX.CALL_ACTIVITY` - The call activity window
-   `RX.CALL_SELECTED` - The currently selected callsign
-   `RX.DIRECTED.ME` - (Disabled) A complete message address to the station callsign
-   `RX.DIRECTED` - A complete message
-   `RX.SPOT` - Generated when a QTH locator is received from a node and the option for sending reports to the Internet is enabled
-   `RX.TEXT` - Contents of the QSO window
-   `STATION.CALLSIGN` - Callsign of the station
-   `STATION.GRID` - Grid locator of the station
-   `STATION.INFO` - QTH/info of the station
-   `STATION.STATUS` - Bug: This can be 2 things; a message with `FREQ`, `DIAL`, `OFFSET`, `SPEED`, and `SELECTED` params, OR.... the "status" from the settings UI, e.g. "IDLE <MYIDLE> VERSION <MYVERSION>"
-   `TX.FRAME` - Something we are sending
-   `TX.TEXT` - Text in the outgoing message window

#### Messages to JS8Call

-   `INBOX.GET_MESSAGES` - Get a list of inbox messages
-   `INBOX.STORE_MESSAGE` - Store a message in the inbox
-   `MODE.GET_SPEED` - Get the TX speed
-   `MODE.SET_SPEED` - Set the TX speed
-   `RIG.GET_FREQ` - Get the current dial freq and offset
-   `RIG.SET_FREQ` - Set the current dial freq and offset
-   `RX.GET_BAND_ACTIVITY` - Get the contents of the band activity window
-   `RX.GET_CALL_ACTIVITY` - Get the contents of the call activity window
-   `RX.GET_CALL_SELECTED` - Get the currently selected callsign
-   `RX.GET_TEXT` - Get the contents of the QSO qindow
-   `STATION.GET_CALLSIGN` - Get the station callsign
-   `STATION.GET_GRID` - Get the station grid
-   `STATION.GET_INFO` - Get the station QTH/info
-   `STATION.GET_STATUS` - Get the station status
-   `STATION.SET_GRID` - Set the station grid
-   `STATION.SET_INFO` - Set the station QTH/info
-   `STATION.SET_STATUS` - Set the station status
-   `TX.GET_TEXT` - Gets the text in the outgoing message box.
-   `TX.SEND_MESSAGE` - Send a message via JS8Call
-   `TX.SET_TEXT` - Sets the text in the outgoing message box, but does not send it.
-   `WINDOW.RAISE` - Focus the JS8Call window
