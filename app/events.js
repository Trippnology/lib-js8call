function emitEvents(js8, packet) {
	// The catch all 'packet' event
	js8.emit('packet', packet);
	// Event based on the type of packet
	js8.emit(packet.type.toLowerCase(), packet);
	// Extra detailed events
	if (packet.type === 'RIG.PTT') {
		// rig.ptt.on and rig.ptt.off
		js8.emit('rig.ptt.' + packet.value, packet);
		// rig.safe_to_tx
		if (packet.value === 'off') {
			js8.tx.getText().then((text) => {
				js8.emit('rig.safe_to_tx', !text);
			});
		}
	}
	// TODO: `RX.DIRECTED.ME` message type exists in the source, but is currently disabled
	if (packet.type === 'RX.DIRECTED' && js8.utils.messageIsToMe(packet)) {
		js8.emit('rx.directed.to_me', packet);
	}
}

module.exports = { emitEvents };
