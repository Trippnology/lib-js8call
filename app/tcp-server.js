const _ = require('lodash');
const net = require('net');

const events = require('./events');

function parse(msg) {
	let output;
	// Sometimes JS8Call sends multiple objects at once (???)
	try {
		output = JSON.parse(msg.toString());
	} catch (err) {
		output = [];
		// Split on newlines
		let split = msg.toString().split(/\r?\n/);
		// Remove falsey values
		split = _.compact(split);
		_.each(split, (item) => {
			try {
				let thing = JSON.parse(item);
				output.push(thing);
			} catch (err) {}
		});
	}
	return output;
}

function init(js8, options) {
	return new Promise((resolve, reject) => {
		let tcp_server = net.createConnection(
			{ host: options.host, port: options.port },
			() => {
				tcp_server.on('error', (err) => {
					js8.emit('tcp.error', err);
				});
				tcp_server.on('data', (data) => {
					let packet = parse(data);
					// Sometimes JS8Call sends multiple objects at once (???)
					if (Array.isArray(packet)) {
						_.each(packet, (item) => {
							js8.utils.log(item);
							events.emitEvents(js8, item);
						});
						return;
					}
					js8.utils.log(packet);
					events.emitEvents(js8, packet);
				});
				tcp_server.on('end', () => {
					js8.emit('tcp.disconnected', 'TCP connection closed');
				});

				return resolve(tcp_server);
			}
		);
		tcp_server.once('error', (err) => {
			return reject(err);
		});
	});
}

module.exports = { init };
