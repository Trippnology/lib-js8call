const _ = require('lodash');
const EventEmitter = require('events');
const maidenhead = require('maidenhead');

const events = require('./events');
const tcp_server_module = require('./tcp-server');
const udp_server_module = require('./udp-server');
const valid_message_types = require('./valid-message-types');

let config = {
	debug: false,
	exit_when_js8call_closed: true,
	get_metadata_at_launch: true,
	persistent: true,
	tcp: {
		auto_reconnect: false,
		enabled: true,
		host: 'localhost',
		port: 2442,
		seconds_between_reconnect: 5,
	},
	udp: { enabled: false, port: 2332 },
};

// Our UDP server
let udp_server;
// Our TCP server
let tcp_server;

// Some cached info used by getters
let cache = {
	connection: {
		tcp: {
			connected: false,
		},
		udp: {
			connected: false,
		},
	},
	rig: {
		// TODO: Check buffer before setting this
		safe_to_tx: true,
		ptt: null,
	},
	station: {
		callsign: 'NOCALL',
		grid: 'unknown',
		info: 'unknown',
		status: 'unknown',
	},
};

//  Our main module
class JS8 extends EventEmitter {
	// Send a raw message to JS8Call
	send(data) {
		return new Promise((resolve, reject) => {
			if (typeof data == 'object') {
				// If the data is not already JSON, we have the opportunity to verify it
				if (!_.includes(valid_message_types.outgoing, data.type)) {
					return reject(data.type + ' is not a valid message type');
				}
				data = JSON.stringify(data);
			}
			tcp_server.write(data + '\n', resolve);
		});
	}

	help = {
		get valid_message_types() {
			return valid_message_types;
		},
	};

	inbox = {
		getMessages() {
			return new Promise((resolve, reject) => {
				js8.once('inbox.messages', (packet) => {
					return resolve(packet.params.MESSAGES);
				});
				JS8.prototype.send.call(this, {
					type: 'INBOX.GET_MESSAGES',
				});
			});
		},
		storeMessage(callsign, text) {
			return new Promise((resolve, reject) => {
				if (!callsign || !text) {
					return reject(
						'You must supply a callsign and text for the message'
					);
				}
				js8.once('inbox.message', (packet) => {
					return resolve(packet);
				});
				JS8.prototype.send.call(this, {
					type: 'INBOX.STORE_MESSAGE',
					params: { CALLSIGN: callsign, TEXT: text },
				});
			});
		},
	};

	mode = {
		getSpeed() {
			return new Promise((resolve, reject) => {
				js8.once('mode.speed', (packet) => {
					return resolve(packet.params.SPEED);
				});
				JS8.prototype.send.call(this, {
					type: 'MODE.GET_SPEED',
				});
			});
		},
		getSpeedDetailed() {
			return new Promise((resolve, reject) => {
				js8.once('mode.speed', (packet) => {
					let speeds = [
						{ setting: 0, name: 'normal' },
						{ setting: 1, name: 'fast' },
						{ setting: 2, name: 'turbo' },
						{ setting: 3, name: 'ultra' },
						{ setting: 4, name: 'slow' },
					];
					return resolve(speeds[packet.params.SPEED]);
				});
				JS8.prototype.send.call(this, {
					type: 'MODE.GET_SPEED',
				});
			});
		},
		// `MODE.SET_SPEED` seems to be buggy in the API. While the UI changes in the dropdown,
		// the mode is not changed, and querying with `MODE.GET_SPEED` always returns the previous speed.
		// A similar issue also affects `STATION.SET_GRID` and `STATION.SET_INFO`
		// See: https://bitbucket.org/widefido/js8call/issues/350/mode-button-text-does-not-update
		setSpeed(speed) {
			return new Promise((resolve, reject) => {
				// Try to convert strings. Helps with commandline args
				if (typeof speed === 'string') {
					speed = Number(speed);
				}
				let valid_speeds = [0, 1, 2, 4];
				if (!speed || !_.includes(valid_speeds, speed)) {
					return reject(
						'You must supply a speed. Either 0 (normal), 1 (fast), 2 (turbo), or 4 (slow) '
					);
				}
				js8.once('mode.speed', (packet) => {
					return resolve(packet.params.SPEED);
				});
				JS8.prototype.send.call(this, {
					type: 'MODE.SET_SPEED',
					params: { SPEED: speed },
				});
			});
		},
	};

	rig = {
		get ptt() {
			return rig.ptt;
		},
		getFreq() {
			return new Promise((resolve, reject) => {
				js8.once('rig.freq', (packet) => {
					return resolve(packet.params);
				});
				JS8.prototype.send.call(this, {
					type: 'RIG.GET_FREQ',
				});
			});
		},
		get safe_to_tx() {
			return cache.rig.safe_to_tx;
		},
		setFreq(options) {
			return new Promise((resolve, reject) => {
				if (!options || typeof options != 'object') {
					return reject(
						'You must supply an object with at least an OFFSET property'
					);
				}
				js8.once('rig.freq', (packet) => {
					return resolve(packet.params);
				});
				JS8.prototype.send.call(this, {
					type: 'RIG.SET_FREQ',
					params: options,
				});
				// `RIG.SET_FREQ` does not trigger a `RIG.FREQ` message, so we have to request one
				JS8.prototype.send.call(this, {
					type: 'RIG.GET_FREQ',
				});
			});
		},
		getPTT() {
			return new Promise((resolve, reject) => {
				/*
				 * TODO: There is no `RIG.GET_PTT` API call right now, so the only
				 * way we know the PTT status, is if we have seen a `RIG.PTT` packet.
				 * This certainly isn't going to work via the cmdline.
				 */
				return resolve(rig.ptt);
			});
		},
	};

	rx = {
		getBandActivity() {
			return new Promise((resolve, reject) => {
				js8.once('rx.band_activity', (packet) => {
					return resolve(packet.params);
				});
				JS8.prototype.send.call(this, {
					type: 'RX.GET_BAND_ACTIVITY',
				});
			});
		},
		getCallActivity() {
			return new Promise((resolve, reject) => {
				js8.once('rx.call_activity', (packet) => {
					return resolve(packet.params);
				});
				JS8.prototype.send.call(this, {
					type: 'RX.GET_CALL_ACTIVITY',
				});
			});
		},
		getCallSelected() {
			return new Promise((resolve, reject) => {
				js8.once('rx.call_selected', (packet) => {
					return resolve(packet.value);
				});
				JS8.prototype.send.call(this, {
					type: 'RX.GET_CALL_SELECTED',
				});
			});
		},
		getText() {
			return new Promise((resolve, reject) => {
				js8.once('rx.text', (packet) => {
					return resolve(packet.value);
				});
				JS8.prototype.send.call(this, {
					type: 'RX.GET_TEXT',
				});
			});
		},
	};

	station = {
		get callsign() {
			return cache.station.callsign;
		},
		get grid() {
			return cache.station.grid;
		},
		get info() {
			return cache.station.info;
		},
		get status() {
			if (typeof cache.station.status == 'object') {
				return cache.station.status.params;
			} else {
				return cache.station.status;
			}
		},
		getMetadata() {
			return new Promise((resolve, reject) => {
				Promise.all([
					this.getCallsign.call(this),
					this.getGrid.call(this),
					this.getInfo.call(this),
					this.getStatus.call(this),
				])
					.then(([callsign, grid, info, status]) => {
						cache.station = { callsign, grid, info, status };
						return resolve(cache.station);
					})
					.catch((err) => {
						console.log('Problem getting station info:');
						console.error(err);
					});
			});
		},
		getCallsign() {
			return new Promise((resolve, reject) => {
				js8.once('station.callsign', (packet) => {
					return resolve(packet.value);
				});
				JS8.prototype.send.call(this, {
					type: 'STATION.GET_CALLSIGN',
				});
			});
		},
		getGrid() {
			return new Promise((resolve, reject) => {
				js8.once('station.grid', (packet) => {
					return resolve(packet.value);
				});
				JS8.prototype.send.call(this, {
					type: 'STATION.GET_GRID',
				});
			});
		},
		setGrid(grid) {
			return new Promise((resolve, reject) => {
				if (!grid || !maidenhead.valid(grid)) {
					return reject('You must supply a valid grid');
				}
				js8.once('station.grid', (packet) => {
					return resolve(packet.value);
				});
				JS8.prototype.send.call(this, {
					type: 'STATION.SET_GRID',
					value: grid,
				});
			});
		},
		getInfo() {
			return new Promise((resolve, reject) => {
				js8.once('station.info', (packet) => {
					return resolve(packet.value);
				});
				JS8.prototype.send.call(this, {
					type: 'STATION.GET_INFO',
				});
			});
		},
		setInfo(info) {
			return new Promise((resolve, reject) => {
				if (!info || typeof info != 'string') {
					return reject('You must supply some info text');
				}
				js8.once('station.info', (packet) => {
					return resolve(packet.value);
				});
				JS8.prototype.send.call(this, {
					type: 'STATION.SET_INFO',
					value: info,
				});
			});
		},
		getStatus() {
			return new Promise((resolve, reject) => {
				js8.once('station.status', (packet) => {
					return resolve(packet.value);
				});
				JS8.prototype.send.call(this, {
					type: 'STATION.GET_STATUS',
				});
			});
		},
		setStatus(status) {
			return new Promise((resolve, reject) => {
				if (!status || typeof status != 'string') {
					return reject('You must supply some status text');
				}
				js8.once('station.status', (packet) => {
					return resolve(packet.value);
				});
				JS8.prototype.send.call(this, {
					type: 'STATION.SET_STATUS',
					value: status,
				});
			});
		},
	};

	tcp = {
		connect: function () {
			return new Promise((resolve, reject) => {
				startTCPServer().then(resolve).catch(reject);
			});
		},
	};

	tx = {
		getText() {
			return new Promise((resolve, reject) => {
				js8.once('tx.text', (packet) => {
					return resolve(packet.value);
				});
				JS8.prototype.send.call(this, {
					type: 'TX.GET_TEXT',
				});
			});
		},
		sendMessage(text) {
			return new Promise((resolve, reject) => {
				if (!text || typeof text != 'string') {
					return reject('You must supply some message text');
				}
				if (!cache.rig.safe_to_tx) {
					return reject('Rig busy');
				}
				const safety_check = function (safety) {
					if (safety) {
						js8.removeListener('rig.safe_to_tx', safety_check);
						return resolve(text);
					}
				};
				js8.on('rig.safe_to_tx', safety_check);
				JS8.prototype.send.call(this, {
					type: 'TX.SEND_MESSAGE',
					value: text,
				});
			});
		},
		setText(text) {
			return new Promise((resolve, reject) => {
				if (!text || typeof text != 'string') {
					return reject('You must supply some message text');
				}
				js8.once('tx.text', (packet) => {
					return resolve(packet);
				});
				JS8.prototype.send.call(this, {
					type: 'TX.SET_TEXT',
					value: text,
				});
			});
		},
	};

	utils = {
		log(content) {
			if (config.debug) {
				console.log(content);
			}
		},
		messageIsToMe(packet) {
			return packet.params.TO === cache.station.callsign;
		},
	};
}
//  The instance we will pass back to the user
const js8 = new JS8();

function startTCPServer() {
	return new Promise((resolve, reject) => {
		tcp_server_module
			.init(js8, config.tcp)
			.then((server) => {
				tcp_server = server;
				cache.connection.tcp.connected = true;
				let address = tcp_server.address();
				address.mode = 'tcp';
				js8.emit('tcp.connected', address);
				// Get some initial info about the station
				if (config.get_metadata_at_launch) {
					js8.station.getMetadata();
				}
				return resolve(address);
			})
			.catch(reject);
	});
}

function startUDPServer() {
	return new Promise((resolve, reject) => {
		udp_server_module
			.init(js8, config.udp)
			.then((server) => {
				udp_server = server;
				cache.connection.udp.connected = true;
				let address = udp_server.address();
				address.mode = 'udp';
				js8.emit('udp.connected', address);
				return resolve(udp_server);
			})
			.catch(reject);
	});
}

function init() {
	if (config.tcp.enabled) {
		startTCPServer().catch((err) => {
			let err_msg =
				'Could not establish a connection to JS8Call. Check JS8Call is running, and the port is not in use by another program.';
			//js8.emit('error', err_msg);
			js8.emit('tcp.error', err_msg);
			// Log directly for cmdline users
			console.log(err_msg);
			if (config.debug) {
				console.error(err);
			}
			if (!config.persistent) {
				// Without a TCP connection, we're goofed. Give up
				process.exit(1);
			}
			return reject(err_msg);
		});
	}

	if (config.udp.enabled) {
		startUDPServer().catch((err) => {
			let err_msg =
				'Could not set up UDP server. Check JS8Call is running, and the port is not in use by another program.';
			//js8.emit('error', err_msg);
			js8.emit('udp.error', err_msg);
			// Log directly for cmdline users
			console.log(err_msg);
			if (config.debug) {
				console.error(err);
			}
			return reject(err_msg);
		});
	}

	// Listen to our own events
	js8.on('close', (packet) => {
		if (config.exit_when_js8call_closed) {
			console.log('JS8Call has been closed. Exiting.');
			tcp_server.close();
			udp_server.close();
			process.exit();
		}
	});

	// Listening for these events means our info will always be up to date
	js8.on('rig.ptt', (packet) => {
		cache.rig.ptt = packet.value === 'on' ? 1 : 0;
	});
	js8.on('rig.safe_to_tx', (safe) => {
		cache.rig.safe_to_tx = safe;
	});
	js8.on('station.callsign', (packet) => {
		cache.station.callsign = packet.value;
	});
	js8.on('station.grid', (packet) => {
		cache.station.grid = packet.value;
	});
	js8.on('station.info', (packet) => {
		cache.station.info = packet.value;
	});
	js8.on('station.status', (packet) => {
		// TODO: API bug - a `STATION.STATUS` packet can mean 2 things
		cache.station.status = packet;
	});
	js8.on('tcp.disconnected', () => {
		cache.connection.tcp.connected = false;
		if (config.tcp.auto_reconnect && config.persistent) {
			let reconnect_timer = setInterval(() => {
				if (!cache.connection.tcp.connected) {
					startTCPServer()
						.then(() => {
							clearInterval(reconnect_timer);
						})
						.catch((err) => {});
				}
			}, config.tcp.seconds_between_reconnect * 1000);
		}
	});
	js8.on('udp.disconnected', () => {
		cache.connection.udp.connected = false;
	});
	// Something went very wrong, but let's try and cope
	process.on('unhandledRejection', (err) => {
		console.log('Unhandled Rejection:');
		console.error(err);
		if (!config.persistent) {
			process.exit(1);
		}
	});
}

module.exports = function (options) {
	options = options || {};
	// Apply any user provided options to our default config.
	// This way, users can omit any options and we still have sensible defaults
	config = _.defaultsDeep(options, config);
	if (config.tcp.enabled && config.udp.enabled) {
		// Throwing an error during init is a bad idea, just warn instead
		console.log(
			'WARNING: Both TCP and UDP interfaces are enabled at the same time. While this does technically work, it will likely lead to duplicate traffic and headaches for you! TCP only is preferred.'
		);
	}
	init();
	return js8;
};
