const dgram = require('dgram');

const events = require('./events');

/*
 * Parses the buffer into a usable JS object.
 * rinfo contains data about the remote app. This may be useful
 * if we are connected to more than 1 instance of JS8Call and we
 * need to differentiate between them, but not using it right now.
 */
function parse(msg, rinfo) {
	return JSON.parse(msg.toString());
}

function init(js8, options) {
	return new Promise((resolve, reject) => {
		let udp_server = dgram.createSocket('udp4');
		udp_server.bind(options.port);
		// One off connection error
		udp_server.once('error', (err) => {
			console.log(`udp_server error:\n${err.stack}`);
			js8.emit('error', err);
			return reject(err);
		});
		udp_server.on('listening', () => {
			return resolve(udp_server);
		});
		udp_server.on('error', (err) => {
			console.log(`udp_server error:\n${err.stack}`);
			js8.emit('error', err);
			js8.emit('udp.error', err);
		});
		udp_server.on('message', (msg, rinfo) => {
			let packet = parse(msg, rinfo);
			events.emitEvents(js8, packet);
			js8.utils.log(packet);
		});
	});
}

module.exports = { init };
