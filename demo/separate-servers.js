/*
 * Keeping UDP and TCP apart
 * This currently doesn't work :(
 */
const js8_tcp = require('../app')({
	//debug: true,
	tcp: { enabled: true },
	udp: { enabled: false },
});

js8_tcp.on('tcp.connected', (connection) => {
	console.log(
		'Server listening %s:%s Mode: %s',
		connection.address,
		connection.port,
		connection.mode
	);
});

js8_tcp.on('ping', (packet) => {
	console.log('TCP Ping %s', packet.params._ID);
	//console.log(packet);
});

const js8_udp = require('../app')({
	//debug: true,
	tcp: { enabled: false },
	udp: { enabled: true },
});

js8_udp.on('udp.connected', (connection) => {
	console.log(
		'Server listening %s:%s Mode: %s',
		connection.address,
		connection.port,
		connection.mode
	);
});

js8_udp.on('ping', (packet) => {
	console.log('UDP Ping %s', packet.params._ID);
	//console.log(packet);
});

process.on('error', (err) => {
	console.log('Something went wrong:');
	console.error(err);
});
