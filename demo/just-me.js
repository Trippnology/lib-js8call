/*
 * Just Me
 * This example onlywatching for messages addressed to you,
 */

const js8 = require('../app')();
// Outside this example, this line would normally be:
// const js8 = require('@trippnology/lib-js8call')();

js8.on('tcp.connected', (connection) => {
	// At this point, we have setup the connection
	console.log(
		'Server listening %s:%s Mode: %s',
		connection.address,
		connection.port,
		connection.mode
	);
	// You can now safely do your thing!
	// MyApp.init();
});

// If you are only interested in messages addressed to you:
js8.on('rx.directed.to_me', (packet) => {
	/*
	 * At this point, you know this is a message to you
	 * such as "M7GMT: VA1UAV SNR -02 ~" and you can act as you wish.
	 */
	doCustomStuff(packet);
});

function doCustomStuff(packet) {
	// Process the data as you wish
	console.log(packet);

	// Later, when you wish to send text back to JS8Call:
	// js8.tx.sendMessage('Your custom text');
	// Or set the text without TXing
	// js8.tx.setText('Your custom text');

	// As js8.tx.sendMessage() returns a promise, you can do stuff once it
	// resolves or rejects, if you need to:
	js8.tx
		.sendMessage('Your custom text')
		.then(() => {
			console.log('TX finished');
		})
		.catch((err) => {
			console.log('Something went wrong:');
			console.error(err);
		});
}
