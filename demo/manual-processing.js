/*
 * Manual Processing
 * This example shows how to listen to the 'packet' event when you
 * want to process everything coming from JS8Call in your own way.
 */

// Pass in any options you wish to set manually.
// Sensible defaults will be provided for anything you omit.
const js8 = require('../app')();
// Outside this example, this line would normally be:
// const js8 = require('@trippnology/lib-js8call')();

// Config options for your app
let config = {
	stats: {
		interval: 60,
	},
};

// Manual processing of incoming packets
function processPacket(packet) {
	switch (packet.type) {
		case 'PING':
			console.log(
				'[Ping] %s v%s',
				packet.params.NAME,
				packet.params.VERSION
			);
			break;
		case 'RIG.FREQ':
			console.log(
				'[Rig] Frequency is %s (%s). Offset: %s',
				packet.params.DIAL,
				packet.params.BAND,
				packet.params.OFFSET
			);
			break;
		case 'RIG.PTT':
			console.log('[Rig] PTT is %s', packet.value);
			break;
		// You can set some types to specifically do nothing here, to suppress
		// them from being displayed by the catch-all "default" case at the bottom
		case 'RX.ACTIVITY':
			break;
		case 'RX.CALL_ACTIVITY':
			stats.data.calls_heard = Object.keys(packet.params);
			break;
		case 'RX.DIRECTED':
			if (js8.utils.messageIsToMe(packet)) {
				// You may want to do further processing on messages to you
				console.log('##### Message to me: #####');
			}
			console.log('[Over] ' + packet.value);
			break;
		case 'RX.SPOT':
			console.log(
				'[Spot] Call: %s SNR: %s',
				packet.params.CALL,
				packet.params.SNR
			);
			break;
		case 'STATION.CALLSIGN':
			console.log('Station Callsign: %s', packet.value);
			break;
		case 'STATION.STATUS':
			break;
		default:
			console.log(packet);
			break;
	}
}

// Your custom app stuff
let stats = {
	data: {
		message_types: {},
		calls_heard: [],
	},
	tally: function (packet) {
		let tally = stats.data.message_types;
		if (tally[packet.type]) {
			tally[packet.type]++;
		} else {
			tally[packet.type] = 1;
		}
	},
	display: function () {
		console.log('########## STATS ##########');
		console.log('Packet types: %s', stats.data.message_types);
		console.log(
			'Calls heard (%s): %s',
			stats.data.calls_heard.length,
			stats.data.calls_heard.sort()
		);
		console.log('Station status: %s', js8.station.status);
	},
};

/*
 * Listen to events and act on them
 */
js8.on('tcp.connected', (connection) => {
	// Sucessfully setup the connection
	console.log(
		'Server listening %s:%s Mode: %s',
		connection.address,
		connection.port,
		connection.mode
	);
});
// The "firehose". Listen to the 'packet' event when you want to
// process everything coming from JS8Call in your own way.
js8.on('packet', (packet) => {
	processPacket(packet);
	stats.tally(packet);
});

// Display some stats at regular intervals
setInterval(() => {
	stats.display();
}, config.stats.interval * 1000);
