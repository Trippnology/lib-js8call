/*
 * Augmented Data
 * Using external services, you can add useful, additional data
 */

const _ = require('lodash');
const callsign = require('callsign/src/node');

const js8 = require('../app')();
// Outside this example, this line would normally be:
// const js8 = require('@trippnology/lib-js8call')();

function addExtendedInfo(data) {
	let calls = Object.keys(data);
	_.each(calls, (call) => {
		let qrz = callsign.getAmateurRadioInfoByCallsign(call);
		if (qrz) {
			delete qrz.areacn;
			data[call].extended = qrz;
		}
	});
	return data;
}

function addDetailedInfo(data) {
	let calls = Object.keys(data);
	_.each(calls, (call) => {
		let qrz = callsign.getAmateurRadioDetailedByCallsign(call);
		if (qrz) {
			delete qrz.areacn;
			data[call].details = qrz;
		}
	});
	return data;
}

js8.on('tcp.connected', (connection) => {
	console.log(
		'Server listening %s:%s Mode: %s',
		connection.address,
		connection.port,
		connection.mode
	);

	js8.rx.getCallActivity().then((activity) => {
		// Pick one station, for demo clarity.
		let station = Object.keys(activity)[0]; // Change this number to try different stations

		console.log('We start with what JS8Call knows...');
		console.log(station, activity[station]);

		console.log('We can add a few details...');
		let extended_activity = addExtendedInfo(activity);
		console.log(station, extended_activity[station]);

		console.log('Or get more in depth...');
		let detailed_activity = addDetailedInfo(activity);
		console.log(station, detailed_activity[station]);

		console.log(
			'(only showing one station, for clarity, but all items have been augmented)'
		);
		//console.log(extended_activity);
		//console.log(detailed_activity);
	});
});

process.on('error', (err) => {
	console.log('Something went wrong:');
	console.error(err);
});
