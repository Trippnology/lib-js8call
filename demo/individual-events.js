/*
 * Individual Events
 * This example shows how to monitor only the events you are interested in.
 */

// Pass in any options you wish to set manually.
// Sensible defaults will be provided for anything you omit.
const js8 = require('../app')();
// Outside this example, this line would normally be:
// const js8 = require('@trippnology/lib-js8call')();

/*
 * Listen for events and act on them
 */

js8.on('tcp.connected', (connection) => {
	// Sucessfully setup the connection
	console.log(
		'Server listening %s:%s Mode: %s',
		connection.address,
		connection.port,
		connection.mode
	);
});

// Only listen to events you are interested in.
js8.on('ping', (packet) => {
	console.log('[Ping] %s v%s', packet.params.NAME, packet.params.VERSION);
});

js8.on('rig.freq', (packet) => {
	console.log(
		'[Rig] Frequency has been changed to %s (%s). Offset: %s',
		packet.params.DIAL,
		packet.params.BAND,
		packet.params.OFFSET
	);
});

// Fired on any change of PTT status
js8.on('rig.ptt', (packet) => {
	console.log('[Rig] PTT is %s', packet.value);
});
// There are also individual events for On and Off:
/*
js8.on('rig.ptt.on', (packet) => {
	console.log('[Rig] PTT is ON');
});
js8.on('rig.ptt.off', (packet) => {
	console.log('[Rig] PTT is OFF');
});
*/

js8.on('rx.directed.to_me', (packet) => {
	console.log('[Message to me] %s', packet.value);
});

js8.on('station.callsign', (packet) => {
	console.log('Station Callsign: %s', packet.value);
});
